# imports
import tdl
import json
# import math
# import random
from random import randint

# inits
SCREEN_DIMS = (56, 56)
PLAY_AREA = (51, 51)
FPS = 0
RUNNING = True
UI_OPACITY = 0.75
main_console = tdl.init(SCREEN_DIMS[0], SCREEN_DIMS[1], "SomeRL")
nap = tdl.Console(PLAY_AREA[0], PLAY_AREA[1])
ui_layer = tdl.Console(SCREEN_DIMS[0], SCREEN_DIMS[1])
hud_layer = tdl.Console(PLAY_AREA[0], PLAY_AREA[1])
# To clarify the difference between all of these:
# main_console: The main console / window of the program, all the other stuff goes on this.
# nap: the map layer, shows the dungeon and cool stuff like that.
# ui_later: (underneath the map layer) parts of the UI that go in the bottom or to the right.
# hud_layer: any parts of the UI that go over / atop the map.
tdl.set_fps(FPS)
entity_list = []
camera_list = []
priority_layer = []
m_pos = (0, 0)
m_cell = (0, 0)
THEMES = json.loads(open('themes.json').read())


# classes and stuff
class Entity:
    """
    A classic, basically represents something with a position and a few other
    properties.

    Can also auto-append itself to the entity list, if required.

    :param char: Character - "@"c or int.
    :param x: x position, int
    :param y: y position, int
    :param fc: foreground colour, (int,int,int)
    :param bc: background colour, (int,int,int)
    :param append: whether it should append itself, bool
    """
    b_camera = None  # bound camera, makes the camera update when I move.

    def __init__(self, char, x=-1, y=-1, fc=(255, 255, 255), bc=(0, 0, 0), append=False):
        self.char = char
        self.x = x
        self.y = y
        self.fc = fc
        self.bc = bc
        if append:
            entity_list.append(self)

    def move(self, dx=0, dy=0):
        """
        Moves the entity by (dx, dy), then causes an update to the entity's bound camera, if it has one.

        :param dx: delta x, int
        :param dy: delta y, int
        :return: None
        """
        self.x, self.y = self.x + dx, self.y + dy
        if self.b_camera:
            self.b_camera.update()

    def draw(self, con=nap):
        """
        Simple draw function, just draws at the entity's x & y, offset by a camera.

        :param con: the console upon which the entity is drawn. Usually the map cam.
        :return: None
        """
        c_size = con.get_size()
        con.draw_char(self.x - main_camera.ex() + c_size[0]//2,
                      self.y - main_camera.ey() + c_size[1]//2,
                      self.char, c_theme.pick_colour(self.fc), c_theme.pick_colour(self.bc))

    def update(self):
        """
        Updates the entity. For now, basically another draw() function but without the con param.

        :return: None
        """
        self.draw()


class Camera:
    """
    Camera object, can have a centre-entity which it will follow to the end of the earth.

    Every draw() is offset by the camera's position.

    :param x: x pos, int
    :param y: y pos, int
    :param entity: bound entity, Entity
    :param ox: offset x, int
    :param oy: offset y, int
    """
    def __init__(self, x=0, y=0, entity=None, ox=0, oy=0):
        self.target = entity
        self.ox = ox
        self.oy = oy
        if entity:
            self.x = entity.x
            self.y = entity.y
            entity.b_camera = self
        else:
            self.x = x
            self.y = y
        camera_list.append(self)

    def move(self, dx=0, dy=0):
        """
        Moves the camera by (dx, dy), then causes an update.

        :param dx: delta x, int
        :param dy: delta y, int
        :return: None
        """
        self.x, self.y = self.x + dx, self.y + dy
        self.update()

    def offset(self, dx=0, dy=0):
        """
        Moves the camera offset position by (dx, dy), then causes an update.

        :param dx: delta x, int
        :param dy: delta y, int
        :return: None
        """
        self.ox, self.oy = self.x + dx, self.y + dy
        self.update()

    def set_offset(self, ox=0, oy=0):
        """
        Sets the camera offset to (ox, oy), then causes an update.

        :param ox: offset x, int
        :param oy: offset y, int
        :return: None
        """
        self.ox, self.oy = ox, oy
        self.update()

    def update(self):
        """
        Verifies the current camera position. (Sets the camera's position to its target's, if it has one.)

        :return: None
        """
        if self.target:
            self.x, self.y = self.target.x, self.target.y

    def ex(self):
        """
        :return: effective x, int
        """
        return self.x + self.ox

    def ey(self):
        """
        :return: effective y, int
        """
        return self.y + self.oy


class ColourTheme:
    """
    A theme of colours. Contains any number of colours, and automatically chooses one based on the target colour.
    An extension to this may need to be made in future, we'll see.
    """
    _colours = []
    _lum_modifiers = (0, 1)  # constant, multiplier

    def __init__(self, name, colours):
        self._colours = colours
        self.name = name
        extremes = (1, 0)  # min, max
        for col in colours:
            l_col = lum(col)
            if l_col < extremes[0]:
                extremes = (l_col, extremes[1])
            if l_col > extremes[1]:
                extremes = (extremes[0], l_col)
        self._lum_modifiers = (extremes[0], (extremes[1] - extremes[0]))

    def pick_colour(self, col):
        """
        Determines which colour to use, based on the difference in brightness, according to the colour's luminance.

        :param col: Target colour
        :return: The colour in the theme which most closely matches the brightness of the target colour.
        """
        d = {}
        m_col = (lum(col) * self._lum_modifiers[1]) + self._lum_modifiers[0]
        low = (0, 1)
        # print(self._lum_modifiers)
        for c in enumerate(self._colours):
            # print(c[1], lum(c[1]), col, m_col, lum(col))
            d[c[0]] = abs(lum(c[1]) - m_col)
        # print(d)
        for i, d in d.items():
            if d < low[1]:
                low = (i, d)
        # print(low)
        return self._colours[low[0]]

    def pick_colour_by_max(self, col):
        """
        Determines which colour to use, based on the difference in brightness, according to the colour's max value.
        Use the default pick_colour(col) instead.

        :param col: Target colour
        :return: The colour in the theme which most closely matches the brightness of the target colour.
        """
        d = {}
        m_col = max(col)
        low = (0, 0)
        for c in enumerate(self._colours):
            d[c[0]] = abs(max(c[1]) - m_col)
        for i, d in d.items():
            if d < low[1]:
                low = (i, d)
        return self._colours[low[0]]


class EmptyTheme:
    """
    An empty theme. All colour picking functions return the target colour exactly.
    """
    name = "none"

    @staticmethod
    def pick_colour(col):
        return col

    @staticmethod
    def pick_colour_by_max(col):
        return col


# inits 2: stuff which will probably be moved, eventually
c_theme = EmptyTheme()
player = Entity('@', 1, 1, c_theme.pick_colour((255, 255, 255)), c_theme.pick_colour((0, 0, 0)))
priority_layer.append(player)
for x in range(400):
    entity_list.append(Entity(1, x % 20, x//20, fc=(randint(0, 255), randint(0, 255), randint(0, 255)),
                              bc=(randint(0, 255), randint(0, 255), randint(0, 255))))
tentiny = Entity(1, 2, 2, c_theme.pick_colour((255, 0, 255)), c_theme.pick_colour((255, 0, 255)))
main_camera = Camera(entity=player)


# global funcs
def lum(col):
    """
    Determines the luminance of a colour, as given by the formula at https://en.wikipedia.org/wiki/Relative_luminance

    :param col: Colour, tuple or list of 3 ints between 0 and 255
    :return: Luminance, float between 0 and 1
    """
    return ((0.2126/255) * col[0]) + ((0.7152/255) * col[1]) + ((0.0722/255) * col[2])


def handle_inputs():
    """Handles tdl events, returns True if it's a quit."""
    evs = tdl.event.get()  # get events
    for ev in evs:  # iterate through events
        print(ev, ev.type)
        if ev.type == "KEYDOWN":
            if ev.keychar != "TEXT":
                if ev.keychar == "KP8" or ev.keychar == "UP":
                    player.move(dy=-1)
                elif ev.keychar == "KP2" or ev.keychar == "DOWN":
                    player.move(dy=1)
                elif ev.keychar == "KP4" or ev.keychar == "LEFT":
                    player.move(dx=-1)
                elif ev.keychar == "KP6" or ev.keychar == "RIGHT":
                    player.move(dx=1)
                elif ev.keychar == "KP1":
                    player.move(-1, 1)
                elif ev.keychar == "KP3":
                    player.move(1, 1)
                elif ev.keychar == "KP7":
                    player.move(-1, -1)
                elif ev.keychar == "KP9":
                    player.move(1, -1)
            else:
                if ev.text == "]":
                    cycle_theme()
        elif ev.type == "MOUSEMOTION":
            global m_pos, m_cell
            m_pos = ev.pos
            m_cell = ev.cell
            main_console.draw_frame(1, 1, 49, 49, 2)
        elif ev.type == "QUIT":
            return True
            # return True on quit, which later ends the main loop.


def load_theme(name):
    global c_theme
    success = False
    if name == "none" or name is None:
        unload_theme()
        return
    for th in THEMES:
        if th['name'] == name:
            c_theme = ColourTheme(th['name'], th['colours'])
            success = True
            break
    if not success:
        print("Theme not found.")
    else:
        main_console.set_colors(c_theme.pick_colour((69, 69, 69)), c_theme.pick_colour((0, 0, 0)))
        ui_layer.set_colors(c_theme.pick_colour((69, 69, 69)), c_theme.pick_colour((0, 0, 0)))
        nap.set_colors(c_theme.pick_colour((69, 69, 69)), c_theme.pick_colour((0, 0, 0)))


def cycle_theme():
    i = None
    for x in range(len(THEMES)):
        print(c_theme.name, i, x, THEMES[x]['name'])
        if c_theme.name == THEMES[x]['name']:
            i = x
            break
    if i is None:
        raise ValueError
    while i+1 >= len(THEMES):
        i -= len(THEMES)
    print(THEMES[i+1], THEMES[i+1]['name'])
    load_theme(THEMES[i+1]['name'])


def unload_theme():
    global c_theme
    print("Unloading theme")
    c_theme = EmptyTheme()
    main_console.set_colors(c_theme.pick_colour((69, 69, 69)), c_theme.pick_colour((0, 0, 0)))
    ui_layer.set_colors(c_theme.pick_colour((69, 69, 69)), c_theme.pick_colour((0, 0, 0)))
    nap.set_colors(c_theme.pick_colour((69, 69, 69)), c_theme.pick_colour((0, 0, 0)))


def lerp(v1, v2, t): return v1 + t*(v2-v1)


def mod(v1):  # simple modulus function, probably can be optimised.
    ans = 0
    for x in v1:
        ans += x ** 2
    return ans ** 0.5


# main loop
print(c_theme.name)
main_console.set_colors(c_theme.pick_colour((69, 69, 69)), c_theme.pick_colour((0, 0, 0)))
ui_layer.set_colors(c_theme.pick_colour((69, 69, 69)), c_theme.pick_colour((0, 0, 0)))
nap.set_colors(c_theme.pick_colour((69, 69, 69)), c_theme.pick_colour((0, 0, 0)))
while RUNNING:
    main_console.clear()
    ui_layer.draw_frame(m_cell[0], 0, 1, SCREEN_DIMS[1], 2)
    ui_layer.draw_frame(0, m_cell[1], SCREEN_DIMS[0], 1, 2)
    ui_layer.draw_char(m_cell[0], m_cell[1], 1, c_theme.pick_colour((255, 0, 0)))
    # q = handle_inputs()
    if handle_inputs(): break  # q:break
    for en in entity_list:
        en.update()
    for en in priority_layer:
        en.draw()
    main_console.blit(nap, 0, 0, SCREEN_DIMS[0], SCREEN_DIMS[1])
    main_console.blit(ui_layer, 0, 0, SCREEN_DIMS[0], SCREEN_DIMS[1], fg_alpha=UI_OPACITY, bg_alpha=0)
    nap.clear()
    ui_layer.clear()
    tdl.flush()
